CC=g++
FUSEFLAGS=$(shell pkg-config fuse --cflags --libs)

CFLAGS=$(FUSEFLAGS) -O0 -lcrypto -pthread -L/usr/local/lib -g -Wall -lboost_serialization

CURLFLAGS=-L/usr/lib/x86_64-linux-gnu -lcurl -Wl,-Bsymbolic-functions

all:
	g++ cache_stats.cc stat_cache.cc prefetch.cc predict.cc util.cc s3.cc pardus.cc $(CFLAGS) $(CURLFLAGS) libs3.a -lxml2 -o pardus

clean:
	rm pardus

unmount:
	fusermount -u mountpoint

mount:
	./pardus -o nonempty mountpoint

debug:
	rm -rf pardus_cache/*
	./pardus -d -o nonempty mountpoint
