#ifndef PREFETCH_H
#define PREFETCH_H

#include "s3.h"
#include "util.h"

#include <vector>
#include <pthread.h>

using namespace std;

//we need some kind of a prefetching thread
class PrefetchWorker {
  public:
    PrefetchWorker(const char *s3_path, PardusS3 *s3_obj, string cd);
       
    static void* prefetch(void *s3_path);
    void work(void);  

  private:
    const char *s3_path;
    pthread_t worker_thread;
    static PardusS3 *s3_obj;
    static string cache_dir;
    
};
#endif
