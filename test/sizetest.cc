#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include <errno.h>

int main (void) {
  struct stat st_buf;

  int wutlol = stat("../mountpoint/YES.txt", &st_buf);

  printf("%d\n", wutlol);

  if(wutlol == -1) {
    printf("%s\n", strerror(errno));
  } 

  printf("Size of YES.txt: %ld\n", st_buf.st_size);

  return 0;
}

