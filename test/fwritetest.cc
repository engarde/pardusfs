#include <stdio.h>

#include <iostream>
using namespace std;

int main (void) {
  FILE *fh = fopen("../mountpoint/YES.txt", "a+");
  
  if(fh == NULL) {
    cout << "UNABLE TO OPEN FILE" << endl;
  }

  char *buffer = "AWESOME SAUCE\n";
  
  fputs(buffer, fh);
  
  return 0;
}
