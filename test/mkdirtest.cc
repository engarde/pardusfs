#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

int main (void) {
  const char *pathname = "../mountpoint/newdir";
  int ret = mkdir(pathname, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if(ret == -1) {
    printf("WHOOPS MKDIR FAILED, ERROR: %s\n", strerror(errno));
  }

  printf("MKDIR SUCCESS\n");

  return 0;
}
