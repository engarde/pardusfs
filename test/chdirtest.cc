#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

int main (void) {
  int ret = chdir("../mountpoint/");

  if(ret == -1) {
    printf("ERROR IN CHDIR: %s\n", strerror(errno));
  }

  printf("chdir success.");

  return 0;
}
