#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

int main (void) {
  DIR *p = opendir("../mountpoint/");
  if(p == NULL) {
    printf("ERROR IN OPENTEST: %s\n", strerror(errno));
  }

  return 0;
}
