#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main (void) {
  int fd = open("../mountpoint/hello.c", O_WRONLY);
  
  if(fd == -1) {
    printf("Whoops, couldn't open that...\n");
  }  

  char *buffer = "NONSENSE IS THIS\n";
  
  int ret = write(fd, buffer, sizeof buffer);

  if(ret == -1) {
    printf("Whoop,s couldn't write to that...\n");
    printf("ERROR string: %s\n", strerror(errno));

  }  
  return 0;
}
