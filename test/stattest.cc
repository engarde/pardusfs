#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include <errno.h>

int main (void) {
  struct stat st_buf;

  int wutlol = stat("../mountpoint/testdir", &st_buf);
  int eerno = errno;

  if(wutlol == -1) {
    printf("ERROR IN STATTEST: %s\n", strerror(errno));
    return 1;
  } 

  printf("%d\n", wutlol);
  printf("MODE: %d\n", st_buf.st_mode);

  return 0;
}

