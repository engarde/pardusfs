#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>

int main (void) {
  int ret = mknod("../mountpoint/new.c", S_IFREG, S_IRUSR|S_IWUSR|S_IXUSR);

  if(ret < 0) {
    printf("MKNOD FAILED! ERROR: %s\n", strerror(errno));
    return 1;
  } 

  printf("MKNOD SUCCESS\n"); 

  return 0;
}
