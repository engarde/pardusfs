#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

int main (void) {
  DIR *p = opendir("../mountpoint/");

  if(p == NULL) {
    printf("ERROR IN READDIRTEST: %s\n", strerror(errno));
  }
  
  struct dirent *dir = readdir(p);
  if(dir == NULL) {
    printf("ERROR IN READDIRTEST: %s\n", strerror(errno));
  }
 
  printf("0\n");
 
  return 0;
}
