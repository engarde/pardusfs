#include "prefetch.h"

string PrefetchWorker::cache_dir = "";
PardusS3 *PrefetchWorker::s3_obj = NULL;

PrefetchWorker::PrefetchWorker(const char *sp, PardusS3 *s3, string cd) {
  this->s3_path = sp;
  this->s3_obj = s3;
  this->cache_dir = cd;
}

void* PrefetchWorker::prefetch(void *s3_path) {
  char *s3_path_str = (char *)s3_path;
  Util::log("prefetch", s3_path_str);

  if(!Util::inCacheAlready(s3_path_str, cache_dir)) { 
    Util::log("prefetch", "started");
    s3_obj->getObjectToFile(s3_path_str, Util::getCachePath(s3_path_str, cache_dir));
  }
  else {
    Util::log("prefetch", "skipped, already in cache");
  }   

  return NULL;
}

void PrefetchWorker::work(void) {
  int rc = pthread_create(&(this->worker_thread), NULL, &prefetch, 
                          (void *)s3_path);  

  if(rc == -1) {
    Util::log("PrefetchWorker::work", "Failed to create new thread!");
  }
}

