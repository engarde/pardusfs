#include "predict.h"
using namespace std;

//used for serialization
//may be the worst code in the world
std::ostream & operator<<(std::ostream &os, const FileChance &fc) {
  return os << ' ' << fc.path << ' ' << fc.probability << ' ' << fc.hitCount << ' ' << fc.totalCount;

}

FileChance::FileChance(void) {
  this->path = "";
  this->probability = 0.0;
  this->hitCount = 0;
  this->totalCount = 0;
}

FileChance::FileChance(string path, double probability) {
  this->path = path;
  this->probability = probability;
  this->hitCount = 0;
  this->totalCount = 0;
}

double FileChance::probabilityComplement(void) {
  return (1-probability);
}

void FileChance::setProbability(void) {
  if(totalCount <= 0) {
    return; //a negative probability doesn't make much sense
  }

  //TODO this typecasting is all BS
  this->probability = (double)((double)hitCount/(double)totalCount);
}

string FileChance::toString(void) {
  stringstream sst;
  sst << "(path: " << path << ", chance: " <<  probability 
  << ", hits: " << hitCount << ", total: " << totalCount << ")";
  return (sst.str());
}

void FileChance::miss(void) {
  this->totalCount++; 
  setProbability();
}

void FileChance::hit(void) {
  this->hitCount++;
  this->totalCount++;
  setProbability();
}

////////////////////////////////////////////////////

DirChance::DirChance() {
  this->dir_path = "";
}

DirChance::DirChance(string p) {
  dir_path = p;
}

string DirChance::toString(void) {
  return dir_path;
}

FileChance DirChance::getHighestChancer(void) {
  double max_chance= -1.0;
  int max_index= -1;

  for(int i = 0; i<probabilities.size(); i++) {
    if(probabilities[i].probability > max_chance) {
      max_chance = probabilities[i].probability;
      max_index = i; 
    }
  }

  return (probabilities[max_index]);
}

void DirChance::printProbabilities(void) {
  cout << "[";
  for(int i = 0; i < probabilities.size(); i++) {
    cout << probabilities[i].toString() << ", ";
  }
  cout << "]" << endl;
}

void DirChance::addFC(string path, double probability) {
  FileChance fp(path, probability);

  this->probabilities.push_back(fp);
}

int DirChance::getFCByPath(string path) {
  for(int i = 0; i < probabilities.size(); i++) {
    if(probabilities[i].path == path) {
      return i;
    }
  }

  //nothing matched
  return -1; 
}

///////////////////////////////////////////////////

//returns an index
int FSChance::getDCByPath(string dir_path) {
  for(int i = 0; i < probabilities.size(); i++) {
    if(probabilities[i].dir_path == dir_path) {
      return i;
    }
  }

  //nothing matched
  return -1;
}

void FSChance::printDirs(void) {
  cout << "{ ";
  for(int i = 0; i < probabilities.size(); i++) {
    cout << probabilities[i].toString() << ", ";
  }
  cout << "} " << endl;
}

void FSChance::addDC(DirChance dc) {
  this->probabilities.push_back(dc);  
}

//print everything down to the individual files
void FSChance::printStructure(void) {
  cout << "COMPLETE STRUCTURE START" << endl;

  cout << "{" << endl;

  for(int i = 0; i<probabilities.size(); i++) {
    DirChance dc = probabilities[i];
    cout << dc.toString() << ": ";
    dc.printProbabilities(); 
  }
  cout << "}" << endl;
 
  cout << "COMPLETE STRUCTURE END" << endl;
} 
