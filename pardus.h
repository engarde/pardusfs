#ifndef PARDUS_H
#define PARDUS_H

/* terrible, horrible hack to get FUSE to work */
#define FUSE_USE_VERSION 26

/* library includes */
#include <fuse.h>

/* standard includes */
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

/* OS includes */
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>

#include <vector>

#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif

/* local includes */

#include "predict.h"
#include "stat_cache.h"
#include "cache_stats.h"

using namespace std;

class Pardus {
  public:
    void init(string, string);
    static int access(const char*, int);
    static int readlink(const char*, char*, size_t);
    static int mknod(const char*, mode_t, dev_t);
    static int mkdir(const char*, mode_t);
    static int symlink(const char*, const char*);
    static int unlink(const char*);
    static int rmdir(const char*);
    static int rename(const char*, const char*);
    static int link(const char*, const char*);    
    static int chmod(const char*, mode_t);
    static int chown(const char*, uid_t, gid_t);   
    static int truncate(const char*, off_t);
    static int open(const char *path, struct fuse_file_info *fi);
    static int read(const char*, char*, size_t, off_t, fuse_file_info*);
    static int write(const char*, const char*, size_t, off_t, fuse_file_info*);    
    static int statfs(const char*, struct statvfs*);

    static int release(const char*, fuse_file_info*);  
    static int fsync(const char*, int, fuse_file_info*);
 
    static int getattr(const char*, struct stat*);

    static int readdir(const char *, void *, fuse_fill_dir_t, off_t, 
    struct fuse_file_info*);

    static int opendir(const char*, struct fuse_file_info *);

    static int releasedir(const char *, struct fuse_file_info *);

    void setOperations(struct fuse_operations *); 
    
    static void* syncThread(void *);
    void initS3Sync(void); 

    int syncWithS3(void);
    int syncWithS3Dir(string root);

    void createDirsInCache(vector<string> dirs);
    void initChances(vector<string> dirs);

    struct fuse_operations pardus_oper;    
      
    static bool need_sync;
    
    static FSChance chances;
    static void hit(const char *s3_path);
    static void missExcept(const char *except);

    //doesn't take s3_path!
    static void prefetchChancer(const char *path);
    
    static void loadChances(void);
    static void dumpChances(void);
    static FSChance getChances(void);
    static void combineChances(void);

    //where to put the serialized "chances" object
    static string serializePath;

    //hold on to object header requests
    //so we don't have to keep looking them up
    static StatCache stat_cache;

  private:
    static string cache_dir;
    static string mount_dir;

    pthread_t s3sync_thread;
    //set when we need sync
    //reset when sync complete
};

string Pardus::cache_dir;
string Pardus::mount_dir;

#endif
