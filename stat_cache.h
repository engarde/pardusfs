#ifndef STAT_CACHE_H
#define STAT_CACHE_H

#include <map>
#include <algorithm>
#include <string>

//for S3ResponseProperties
#include "s3.h"

using namespace std;

class StatCache {
  
  public:
    //map from path from to header properties (e.g. file size)
    map<string, S3ResponseProperties*> cache;
  
    bool isPresent(string s3_path);
};

#endif
