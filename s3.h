#ifndef S3_H
#define S3_H 

#include <curl/curl.h>

#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/md5.h>

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <string>

#include "libs3.h"

//in seconds
#define MAX_WAIT 3

using namespace std;

static const EVP_MD* evp_md = EVP_sha1();

typedef struct {
  char *data;
  int length;
}PutData;

typedef struct {
  //S3ListBucketContent *list;
  vector<string> *list;
  int list_length;
}ObjectList;

class PardusS3 {
  public:
    void init(string bucketName, string asak, string aaki); 

    string calcSignature(string method, string content_type, string date, curl_slist *headers, string resource);

    void setBucketName(string);
    string getBucketName(void);

    string getHostURL(string);
    string getDateString(void);
    curl_slist* setGeneralHeaders(curl_slist *headers);
    string getAuthorizationHeader(string objectpath, string content_type, curl_slist *headers);

    static int putObjectDataCallback(int, char*, void*); 
    static S3Status responsePropertiesCallback(const S3ResponseProperties *properties, void *callbackData);

    static void responseCompleteCallback(S3Status, const S3ErrorDetails*, void*);


    void resetS3Status(void);

    S3ResponseProperties* objectHeaders(string);
    int getObject(string, long bytes, S3Status (*)(int, const char *, void*));
    void deleteObject(string, string);
    void putObject(string, string, string);
  
    char* getObjectBlocking(string objectpath, long bytes);
    static S3Status getObjectBlockingCallback(int x, const char * string, void * callback_data);

    int getObjectToFile(string objectpath, string filepath);
    static S3Status getObjectToFileCallback(int buf_size, const char *buf, void * callback_data);

    static S3Status listBucketCallback(int isTrun, const char *nextMarker,
                                      int contentsCount,
                                      const S3ListBucketContent *contents,
                                      int commonPrefixesCount,
                                      const char **commonPrefixes,
                                      void *callbackData);
    
    //TODO both of these methods need refactoring pronto 
    vector<string> listObjectsBlocking(void);
    //this is just non-functional
    ObjectList* listObjectsPrefix(string prefix);

    vector<string> cachedObjectList;

    static void teardown(void);

  private:
    string awsSecretAccessKey;
    string awsAccessKeyId;
    string bucketName;

    static S3Status statusG;
    static S3Protocol protocolG;
    static S3UriStyle uriStyleG;

    //this is used to block on    
    static S3ResponseProperties* response;

    //also used to block on
    static char *buffer;
    static int bufsize;

    //again, this is blocked on so don't mess with it
    static S3ListBucketContent *content_list;
    static vector<S3ListBucketContent> content_list_vector;
    static vector<int> content_length_vector;
    static int content_list_length;
    static vector<string> test_vector;
};

#endif
