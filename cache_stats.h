#ifndef CACHE_STATS_H
#define CACHE_STATS_H

#include <string>
#include <sstream>

using namespace std;

class CacheStats {
  public:
    static int cacheHits;
    static int cacheMisses;
    
    static void hit(void);
    static void miss(void);
    
    static string toString();
};

#endif
