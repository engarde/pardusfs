#include "stat_cache.h"

bool StatCache::isPresent(string s3_path) {

  if(cache.find(s3_path) == cache.end()) {
    return false;       
  } 

  return true;
}

