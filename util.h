#ifndef UTIL_H
#define UTIL_H 

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <curl/curl.h>

#include <sys/stat.h>
#include <stdio.h>

#include <vector>

#include "s3.h"
#include "cache_stats.h"

using namespace std;

class Util {

  public:
    static void init(void);
  
    static string urlEncode(const string&);
    static void log(string, string);

    static string getDate();

    static string longToString(long);

    static string charStarToString(char *);

    static string headersToString(curl_slist *headers);

    static void logPath(string prefix, const char *path);

    static char* getLocalPath(const char *path);
  
    static char *getCachePath(const char *path, string cache_dir);

    static char* getMountPath(const char *path, string mount_dir);

    static char* getS3Path(const char *path);

    static int stringToFile(const char *path, const char *buffer);

    static bool fileExists(const char *path);
    static bool inCacheAlready(const char *s3_path, string cache_dir);

    static void objectListToVector(ObjectList *ol, vector<string>& keys);

    static char* slurpFile(const char *path);
  
    static void getDirectoriesFromOL(vector<string> list, vector<string>& dirs); 
    
    static void getFilesWithPrefix(string prefix, vector<string> list,
                              vector<string>& files);

    static void prefetch(const char *s3_path, string cache_dir, PardusS3 *s3);

    static string getDirFromFile(const char *s3_path);
    static int countSlashes(const char *path);

    static bool prefetchSwitch;

  private:
    static ofstream logfile;
};

#endif
