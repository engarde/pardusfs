#ifndef PREDICT_H
#define PREDICT_H

#include <vector>
#include <cstddef>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>

#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/assume_abstract.hpp>

using namespace std;

class FileChance {
  friend std::ostream & operator<<(std::ostream &os, const FileChance &fc);
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* file version */) {
    ar & path & probability & hitCount & totalCount;
  }

  public:
     
    //path
    string path;

    //a probability associated with it
    double probability;

    //counts
    int hitCount;
    int totalCount;

    FileChance(string path, double probability);
    FileChance();

    void hit(void);
    void miss(void);

    //use hitCount and totalCount to set the probability
    void setProbability(void);
    double probabilityComplement(void);
    string toString(void);
};

class DirChance {
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & dir_path;
    ar & probabilities;
  }  

  public:
    //path to directory
    string dir_path;
    
    //series of probabilities for each file
    vector<FileChance> probabilities;

    DirChance(string dir_path);
    DirChance();
    string toString(void);
    void addFC(string path, double probability);
    int getFCByPath(string path);
    void printProbabilities(void);
    FileChance getHighestChancer(void);
};

class FSChance {
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {

    ar & probabilities;

  }  

  public:
    vector<DirChance> probabilities;
    
    void addDC(DirChance);
    int getDCByPath(string dir_path);
    void printDirs(void);
    void printStructure(void);
};

#endif
