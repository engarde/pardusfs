#include "util.h"

#ifndef PREFETCH_H
#include "prefetch.h"
#endif

using namespace std;

ofstream Util::logfile;
bool Util::prefetchSwitch = true;

void Util::init(void) {
  (Util::logfile).open("pardus.log");
}

string Util::urlEncode(const string &s) {
  static const char hexAlphabet[] = "0123456789ABCDEF";

  string result;

  for (unsigned i = 0; i < s.length(); ++i) {
    if (s[i] == '/') // Note- special case for fuse paths...  result += s[i]; else if (isalnum(s[i]))
      result += s[i];
    else if (s[i] == '.' || s[i] == '-' || s[i] == '*' || s[i] == '_')
      result += s[i];
    else if (s[i] == ' ')
      result += '+';
    else {
      result += "%";
      result += hexAlphabet[static_cast<unsigned char>(s[i]) / 16];
      result += hexAlphabet[static_cast<unsigned char>(s[i]) % 16]; }
  }

  return result;
}

string Util::getDate(void) {
  char buf[100];
  time_t t = time(NULL);

  strftime(buf, sizeof(buf), "%a, %d %b %Y %H:%M:%S GMT", gmtime(&t));

  return buf;
}

void Util::logPath(string prefix, const char *path) {
  string path_str;
  path_str = path;

  Util::log(prefix, "path: " + path_str); 
}

void Util::log(string prefix, string log) {
  logfile << prefix << ": " << log << endl;
  logfile.flush();
}

string Util::longToString(long bytes) {
  stringstream sst;
  string result;

  sst << bytes;
  sst >> result;

  return result;
}

string Util::charStarToString(char *str) {
  stringstream sst;
  string result;

  sst << str;
  sst >> result;
  
  return result;
}

string Util::headersToString(curl_slist *headers) {
  curl_slist *top = headers;
  string result;

  while(top->next != NULL) {
    result += charStarToString(top->data);         
    result += ", ";
    top = top->next;
  }

  return result;
}

char* Util::getLocalPath(const char *path) {
  string res_path (path); 
  res_path[0] = '/';

  return ((char *)res_path.c_str());  
}

char* Util::getS3Path(const char *path) {
  string res_path (path);
  res_path.erase(0, 1);

  //the 8 is for good measure
  char *ret = (char *)malloc(strlen(res_path.c_str()) + 8);  

  strcpy(ret, res_path.c_str());

  return ret;
}

char* Util::getCachePath(const char *s3_path, string cache_dir) {
  stringstream sst;

  sst << cache_dir;
  sst << "/";
  sst << s3_path;

  return ((char *)sst.str().c_str());
}

char* Util::getMountPath(const char *path, string mount_dir) {
  char *local_path = Util::getLocalPath(path);
  stringstream sst;
  sst << mount_dir;
  sst << "/";
  sst << local_path;
  return ((char *)sst.str().c_str());
}

/*
void Util::objectListToVector(ObjectList *ol, vector<string>& keys) {
  int i;
  for(i = 0; i<ol->list_length; i++) {
    keys.push_back((ol->list)[i]);
  } 
}*/

int Util::stringToFile(const char *path, const char *buffer) {
  FILE *f = fopen(path, "w+");

  if(f == NULL) {
    return -1;
  }

  int ret = fprintf(f, "%s", buffer);

  if(ret == -1) {
    return -1;
  }

  fclose(f);

  return 0;
}

bool Util::fileExists(const char *path) {
  struct stat buf;
  if(stat(path, &buf) != -1) {
    return true;
  }
  return false;
}

bool Util::inCacheAlready(const char *s3_path, string cache_dir) {
  stringstream sst;
  sst << cache_dir;
  sst << "/";
  sst << s3_path;

  bool ret = fileExists(sst.str().c_str());
 
  //keep accounts of cache hits and misses 
  if(ret) {
    Util::log("inCacheAlready", "cache hit!");

    CacheStats::hit();
  } 
  else {
    Util::log("inCacheAlready", "cache miss!");
    CacheStats::miss();
  }

  return ret;
}

char* Util::slurpFile(const char *path) {
  FILE *fh = fopen(path, "rb"); 
  if(fh == NULL) {
    return NULL;
  }
  fseek(fh, 0, SEEK_END);
  long file_size = ftell(fh);
  rewind(fh);  

  char *buffer = (char *) malloc(file_size * (sizeof (char)));
  memset(buffer, 0, file_size);
 
  int bytesWritten = fread(buffer, 1, file_size, fh);
  
  if(bytesWritten != file_size) {
    cerr << "could not slurp file!" << endl;
    return NULL;
  }
 
  return buffer; 
}

//TODO incredibly fragile code - will break as soon as S3 decides not to put 
//a / after directory names
void Util::getDirectoriesFromOL(vector<string> list, vector<string>& dirs) {
  for(int i = 0; i < list.size(); i++) { 
    char *name = (char *)(list[i].c_str());
    cout << "NAME: " << name << endl;
 
    //check if the last character is a forward slash
    if(name[strlen(name)-1] == '/') {
      cout << "GOT DIR: " << name << endl;
      dirs.push_back(name);
    } 
  }
}

void Util::getFilesWithPrefix(string prefix, vector<string> list,
                              vector<string>& files) {
  for(int i = 0; i<list.size(); i++) {
    string filename(list[i]);
    if(filename.find(prefix) == 0) {
      //found prefix at the start of filename

      //get rid of the prefix
      filename = filename.replace(0, prefix.length(), "");
      if(filename != "") {
        files.push_back(filename);
      }
    }
  }
} 

//simple prefetch function
//puts files in the cache
void Util::prefetch(const char *s3_path, string cache_dir, PardusS3 *s3) {
  Util::log("prefetch", s3_path);
 
  if(prefetchSwitch) { 
    PrefetchWorker pw(s3_path, s3, cache_dir);
    pw.work();
  }
 
}

int Util::countSlashes(const char *path) {
  string f(path);
  return (count(f.begin(), f.end(), '/'));
}

string Util::getDirFromFile(const char *s3_path) {
  string directory;
  string file_path(s3_path);
  int last_slash = file_path.rfind('/');

  if(std::string::npos != last_slash) {
    directory = file_path.substr(0, last_slash);
  }  
   
  return (directory+"/");
}

