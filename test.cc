#include <stdio.h>

void foo(int *p) {
  p = NULL;
}

int main (void) {
  int x = 14;
  int *p = &x;
  foo(p);

  printf("%p\n", p);
  
  return 0;
}
