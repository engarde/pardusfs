#include "pardus.h"
#include "prefetch.h"

#ifndef UTIL_H
#include "util.h"
#endif

#include "libs3.h"

//how many seconds between each sync?
#define SYNC_TIME 15

//NO SLASH AT THE END!
#define CACHE_DIR "/home/dhaivat/dev/pardusfs/pardus_cache"
#define MOUNT_DIR "/home/dhaivat/dev/pardusfs/mountpoint"
#define SERIALIZE_PATH "/home/dhaivat/dev/pardusfs/chance_dump"

using namespace std;

Util *utilities = NULL;
Pardus *pardus = NULL;
PardusS3 *s3 = NULL; 
bool Pardus::need_sync = false;
FSChance Pardus::chances;
string Pardus::serializePath(SERIALIZE_PATH);
StatCache Pardus::stat_cache;

int Pardus::access(const char* path, int) {
  Util::log("access", path);
  return 0;
}

int Pardus::release(const char* path, fuse_file_info*){
  Util::log("release", path);
  return -1;
}  

int Pardus::fsync(const char* path, int fd, fuse_file_info*){
  Util::log("fsync", path);
 
  //a stub method, can be left unimplemented according
  //to FUSE docs
 
  return 0;
}

int Pardus::readlink(const char* path, char*, size_t){
  Util::log("readlink", path);
  return -1;
}

int Pardus::mknod(const char* path, mode_t mode, dev_t dev){
  Util::log("mknod", path);

  const char *s3_path = Util::getS3Path(path);
  const char *cache_path = Util::getCachePath(s3_path, cache_dir);

  Util::log("mknod", cache_path);
  Util::log("mknod", s3_path);
 
  int ret = ::mknod(cache_path, mode, dev); 

  if(ret < 0) {
    return -errno;
  }

  return 0; 
}

int Pardus::mkdir(const char* path, mode_t mode){
  Util::log("mkdir", path);

  const char *s3_path = Util::getS3Path(path);
  const char *cache_path = Util::getCachePath(s3_path, cache_dir);
   
  int ret = ::mkdir(cache_path, mode);

  if(ret < 0) {
    return -errno;
  }

  //gotta push a sync, otherwise dirs don't show up
  need_sync = true;

  return 0;
}

int Pardus::symlink(const char* path, const char*){
  Util::log("symlink", path);
  return -1;
}

int Pardus::unlink(const char* path){
  Util::log("unlink", path);

  const char *s3_path = Util::getS3Path(path);
  const char *cache_path = Util::getCachePath(s3_path, cache_dir);

  s3->deleteObject(s3_path, "text/plain"); 

  if(Util::inCacheAlready(s3_path, cache_dir) == true ) {
    ::unlink(cache_path);
  }

  return 0;
}

int Pardus::rmdir(const char* path){
  Util::log("rmdir", path);
  return -1;
}

int Pardus::rename(const char* path, const char*){
  Util::log("rename", path);
  return -1;
}

int Pardus::link(const char* path, const char*) {
  Util::log("link", path);
  return -1;
}    

int Pardus::chmod(const char* path, mode_t){
  Util::log("chmod", path);
  return -1;
}

int Pardus::chown(const char* path, uid_t, gid_t){
  Util::log("chown", path);
  return -1;
}   

int Pardus::truncate(const char* path, off_t size){
  Util::log("truncate", path);
  int res;
 
  const char *s3_path = Util::getS3Path(path);
  const char *cache_path = Util::getCachePath(s3_path, cache_dir);

  cout << "S3_PATH (truncate): " << s3_path << endl;
  cout << "CACHE_PATH (truncate): " << cache_path << endl;
 
  res = ::truncate(cache_path, size);

  if (res == -1) {
    return -errno;
  }

  return 0;
}

int Pardus::write(const char* path, const char* buf, size_t size, off_t offset, fuse_file_info* fi) {

  Util::log("write", path);

  int fd;
  int res;

  const char *s3_path = Util::getS3Path(path);
  const char *cache_path = Util::getCachePath(s3_path, cache_dir);

  (void) fi;
  fd = ::open(cache_path, O_WRONLY);

  cout << "BUFFER (WRITE): " << buf << endl;
  cout << "S3_PATH (WRITE): " << s3_path << endl; 
  cout << "CACHE_PATH (WRITE): " << cache_path << endl;
  cout << "FD (WRITE) : " << fd << endl;

  if(fd == -1) {
    cout << "OPEN failed..." << endl;
    return -errno;
  }

  res = pwrite(fd, buf, size, offset);
  if(res == -1) {
    cout << "PWRITE failed..." << endl;
    res = -errno;
  }

  ::fsync(fd);

  ::close(fd);
 
  need_sync = true; 

  return res;
}    

int Pardus::statfs(const char* path, struct statvfs*){
  Util::log("statfs", path);
  return -1;
}

 
void Pardus::init(string cd, string md) {
  cache_dir = cd;
  mount_dir = md;

  pardus_oper.getattr = getattr;
  pardus_oper.access = access;
  pardus_oper.readlink = readlink;
  pardus_oper.readdir = readdir;
  pardus_oper.mknod = mknod;
  pardus_oper.mkdir = mkdir;
  pardus_oper.symlink = symlink;
  pardus_oper.unlink = unlink;
  pardus_oper.rmdir = rmdir;
  pardus_oper.rename = rename;
  pardus_oper.link = link;
  pardus_oper.chmod = chmod;
  pardus_oper.chown = chown;
  pardus_oper.truncate = truncate;
  pardus_oper.open = open;
  pardus_oper.read = read;
  pardus_oper.write = write;
  pardus_oper.statfs = statfs;
  pardus_oper.release = release;
  pardus_oper.fsync = fsync;
}

int Pardus::read(const char* path, char* buf, size_t size, off_t offset, fuse_file_info* fi) {

  Util::logPath("read", path);
  int ret = 0;

  ret = pread(fi->fh, buf, size, offset);
  
  if(ret < 0) {
    Util::log("read", "ERROR IN READING");
  }

  Util::logPath("read", Util::longToString(ret).c_str());

  return ret; 
} 

void Pardus::missExcept(const char *except) {
  string dir_path = Util::getDirFromFile(except);
  int dc_index = chances.getDCByPath(dir_path);
  if(dc_index == -1) {
    Util::log("miss ERROR", "dc index error!");
    return;
  }

  string except_str(except);

  //TODO NEED TO REFACTOR THIS CODE!
  for(int i = 0; i<chances.probabilities[dc_index].probabilities.size(); i++) {
    if(except_str != chances.probabilities[dc_index].probabilities[i].path) {
      chances.probabilities[dc_index].probabilities[i].miss();
    }    
  }
}

void Pardus::hit(const char *s3_path) {
  Util::log("hit", s3_path);
  string dir_path = Util::getDirFromFile(s3_path);

  int dc_index = chances.getDCByPath(dir_path);
  if(dc_index == -1) {
    Util::log("hit ERROR", "dc index error!");
    return; //TODO have to do some actual error checking here
  }

  int fc_index = chances.probabilities[dc_index].getFCByPath(s3_path);  
  if( fc_index == -1) {
    Util::log("hit ERROR", "fc index error!");
    return;
  }  

  chances.probabilities[dc_index].probabilities[fc_index].hit();

}

/*
Figure out how file handles should be handled, because we don't have
the benefit of the underlying filesystem if we're only holding stuff in mem.

Otherwise, just download the file to the given path
*/
int Pardus::open(const char *path, struct fuse_file_info *fi) {
  cout << "OPEN CALLED!!!!!" << endl;

  Util::log("open", path);

  const char *s3_path = Util::getS3Path(path);
  //record the hit in the statistics
  hit(s3_path);
  //"miss" (i.e. lower the probabilities) of the others 
  // that are in the same directory
  missExcept(s3_path);

  chances.printStructure();
  
  int fd = -1;
   
  if(strcmp(s3_path, path) == 0) {
    return -1; //no, you cannot open() parent directory
  }
  else {
    //if already in cache, don't download
    if(Util::inCacheAlready(s3_path, cache_dir)) {
      fd = ::open(Util::getCachePath(s3_path, cache_dir), fi->flags); //call open(), not Pardus::open()
      fi->fh = fd;
      cout << "S3_PATH: " << s3_path << endl;

      return 0;
    }

    else {
      string s3_path_str(s3_path);
 
      cout << "S3_PATH_STR: " << s3_path_str << endl;
      cout << "NOT IN CACHE!" << endl;

      s3->getObjectToFile(s3_path, Util::getCachePath(s3_path, cache_dir));

      fd = ::open(Util::getCachePath(s3_path, cache_dir), fi->flags);
      fi->fh = fd;

      return 0;
    }
    if(fd == -1) {
      cout << "OH NO! SOMETHING HAS GONE TERRIBLY WRONG IN OPEN()" << endl;
      cout << "ERROR: " << strerror(errno) << endl;
    }
    
  }

  return -1;
}

int Pardus::getattr(const char *path, struct stat* buf) {

  cout << "_____________________________________" << endl;

  Util::log("getattr", path);
     
  int res = 0;
  const char *s3_path_str = Util::getS3Path(path);
  
  //use cached to save bandwidth?
  vector<string> objectNames = s3->cachedObjectList;

  string needle(s3_path_str);

  //Util::objectListToVector(s3->cachedObjectList, objectNames);

  cout << "PATH: " << path << endl; 
  cout << "NEEDLE: " << needle << endl;
  //TODO there is something very screw-y with the s3_path and it keeps
  //getting overrun
  cout << "S3_PATH (GETATTR): " << s3_path_str << endl; 

  const char *cache_path = Util::getCachePath(needle.c_str(), cache_dir);
  int in_cache = Util::inCacheAlready(needle.c_str(), cache_dir);
  /*
  struct stat st_buf;
  if(::stat(cache_path, &st_buf) != -1) {
    in_cache = 1;
  }*/


  Util::log("getattr", "In cache: " + Util::longToString(in_cache));

  buf->st_dev = 22;
  buf->st_ino = 2;
  //buf->st_mode = 16877;
  //buf->st_mode = S_IFDIR;
  if(needle == "") {
    buf->st_mode = S_IFDIR;
  }
 
  //if it is in the cache, just use the underlying
  //filesystem's stat() 
  else if(in_cache == 1) {
    res = ::stat(cache_path, buf);
  }

  else {
    cout << "REG FILE";
    buf->st_mode = 33204;

    //check if the file is present in S3
    if(std::find(objectNames.begin(), objectNames.end(), needle) == objectNames.end() && !in_cache) {
      //the file is not available
      cout << "WHOOPS FILE NOT AVAILABLE, S3_PATH: " << needle << endl;
      return -ENOENT;
    }
  }

  buf->st_nlink = 158;
  buf->st_uid = 0;
  buf->st_gid = 0;
  buf->st_rdev = 0;

 
  //if its in the cache or stat_cache just use that
  //size doesn't matter *that* much so we had
  //might as well not query unless we absolutely
  //have to!
  if(stat_cache.isPresent(s3_path_str)) {
    S3ResponseProperties *properties = stat_cache.cache[s3_path_str];

    if(properties == NULL) {
      return -1;
    } 
    buf->st_size = properties->contentLength;
    buf->st_blksize = 4096;
    buf->st_blocks = 512; 
  }

  else {
    if(Util::inCacheAlready(s3_path_str, cache_dir)) {
      struct stat st_buf;
      ::stat(Util::getCachePath(s3_path_str, cache_dir), &st_buf);

      buf->st_size = st_buf.st_size;
      buf->st_blksize = st_buf.st_blksize;
      buf->st_blocks = st_buf.st_blocks;
    }

    else {
      S3ResponseProperties *properties = s3->objectHeaders(s3_path_str);

      if(properties != NULL) {
        stat_cache.cache[s3_path_str] = properties;

        buf->st_size = properties->contentLength;
        buf->st_blksize = 4096;
        buf->st_blocks = 512; 
      }
      else {
        //something went wrong with S3, who cares what it was?
        return -1;
      }
    }
  }
  /*
  if(strcmp(path, "") == 0) {
    //TODO sizes are totally random
    buf->st_size = 123123;
    buf->st_blksize = 4096;
    buf->st_blocks = 512; 
  }

  else {
    struct S3ResponseProperties *response = s3->objectHeaders(s3_path);

    if(response == NULL) {
      return -1; //file not found
    }

    buf->st_size = response->contentLength;
    buf->st_blksize = 4096;
    buf->st_blocks = (int)(response->contentLength/512);  //TODO this might be nonsense
  }*/

  buf->st_atime = time(NULL);
  buf->st_mtime = time(NULL);
  buf->st_ctime = time(NULL);

  cout << "_____________________________________" << endl;
  return res;

  /*
  int res;
  res = lstat(Util::getLocalPath(path), buf);

  if(res == -1) {
    return -errno;
  }

  return 0;*/
}

int Pardus::readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset,  struct fuse_file_info* ffi) {
  Util::log("readdir", path);

  //PREDICTION METHOD - if we read a directory
  //prefetch the file with the highest probability 
  prefetchChancer(path);

  const char *s3_path = Util::getS3Path(path);
  string s3_path_str(s3_path);

  //TODO this only allows for flat directory structure
  if(strcmp(s3_path, "") != 0) {
    //ObjectList *ol = s3->listObjectsBlocking();
    vector<string> flist = s3->listObjectsBlocking();

    string s3_path_str (s3_path);

    string s3_dir_str = s3_path_str + "/";

    vector<string> files;
    Util::getFilesWithPrefix(s3_dir_str, flist, files);

    for(int i = 0; i < files.size(); i++) {
      cout << "ADDED: " << files[i].c_str() << endl;
      int occurrences = count(files[i].begin(), files[i].end(), '/');
      int f_length = files[i].length();
      int stat_f;

      if(occurrences != 0 && occurrences > 1) {}
      else if(occurrences == 1 && files[i][f_length-1] != '/') {}
      else {
       stat_f = filler(buf, files[i].c_str(), NULL, 0);
      }

      if(stat_f != 0) {
        return -ENOMEM;
      }
    }

    return 0;
  }

  else {
    //ObjectList *ol = s3->listObjectsBlocking();
    vector<string> flist = s3->listObjectsBlocking();

    for(int i = 0; i<flist.size(); i++) {
      string fen = flist[i];
      if(fen != "") {
        string fname = flist[i];

        int occurrences = count(fname.begin(), fname.end(), '/');

        if(occurrences != 0 && occurrences > 1) {
          continue; //skip over directory contents
        }
        else if(occurrences == 1 && fname[fname.length()-1] != '/') {
          continue;
        }
        else {
        }

        //if its a directory, drop the slash off the end
        if(fname[fname.length() - 1] == '/') {
          fname = fname.substr(0, fname.size()-1); 
        }
  
        int stat_f = filler(buf, fname.c_str(), NULL, 0);
        if(stat_f != 0) {
          return -ENOMEM;
        }
      }
    }
 
    return 0;
  }
}

void Pardus::prefetchChancer(const char *path) {
  Util::log("prefetchChancer", path);

  //crux of the cd -> prefetch cache system
  const char *s3_path = Util::getS3Path(path);
  string s3_path_str(s3_path);

  Util::log("prefetchChancer", "S3 Path: " + s3_path_str);

  string upper_dir = s3_path_str + "/";
  Util::log("prefetchChancer", "Upper dir: " + upper_dir);

  int dc_index = chances.getDCByPath(upper_dir);
  string dc_index_str = Util::longToString(dc_index);
  Util::log("prefetchChancer", "DC Index: " + dc_index_str);

  if(dc_index == -1) {
    Util::log("prefetchChancer ERROR", "could not initiate prefetch, dc_index error");
  }

  FileChance highest_chancer = chances.probabilities[dc_index].getHighestChancer();

  //fetch the one that has had the highest chance historically
  Util::prefetch(highest_chancer.path.c_str(), cache_dir, s3); 
}

int Pardus::opendir(const char *path, struct fuse_file_info * fi) {
  Util::log("opendir", path);

  prefetchChancer(path);
  
  return 0;
}

int Pardus::releasedir(const char *path, struct fuse_file_info *) {

  Util::log("releasedir", path);
  return -1;
}

int Pardus::syncWithS3Dir(string root) {
  //comb through cache_directory (look at all the files)
  //TODO include directories here
  cout << "SYNCWITHS3DIR" << endl;

 

  string root_in_cache = cache_dir+root;

  DIR *dirp = ::opendir(root_in_cache.c_str());
  struct dirent *file;

  if(dirp == NULL) {
    Util::log("SYNCWITHS3DIR", "UNABLE TO SYNC -- CANNOT OPEN CACHE DIR! opendir: " + root_in_cache);
    return -1;
  } 

  while((file = ::readdir(dirp)) != NULL) {
    string filename(file->d_name);
    Util::log("SYNCWITHS3DIR", "FILENAME: " + filename);

    string file_cache_path = Util::getCachePath(file->d_name, root_in_cache);
    Util::log("SYNCWITHS3DIR", "FILE_CACHE_PATH: " + file_cache_path);

    struct stat st;
    lstat(file_cache_path.c_str(), &st);
   
    char *buffer = "";

    //standard directories... 
    if(filename == ".." || filename == ".") {
      continue;
    }

    if(S_ISDIR(st.st_mode)) { 
      Util::log("SYNCWITHS3DIR", "----Found directory: " + file_cache_path);

      filename += "/"; //make sure we recognize this as a directory
                       //later
      //RECURSION - loop through this directory too!
      
      cout << "RECURSING! filename: " << root + filename << endl;
      syncWithS3Dir(root+"/"+filename);
    }   

    else {
      Util::log("SYNCWITHS3DIR", "----Found file: " + file_cache_path);

      string slurp_path = Util::getCachePath(filename.c_str(), cache_dir+root);

      buffer = Util::slurpFile(slurp_path.c_str());

      if(buffer == NULL) {
        cout << "UNABLE TO SYNC -- CANNOT SLURP FILE: " << file->d_name << endl;
        buffer = ""; //hackjob fix
      }
    }
    string put_path = root+filename;

    if(put_path[0] == '/') {
      put_path.erase(0, 1); //hack to get rid of leading slash
    }

    Util::log("SYNCWITHS3DIR", "S3_PATH to put: " + put_path);

    s3->putObject(buffer, put_path, "text/plain"); 
    Util::log("SYNCWITHS3DIR", "-----------------------------");
  }
   
  return 0;
}

int Pardus::syncWithS3(void) {
  //comb through cache_directory (look at all the files)
  //TODO include directories here
  vector<string> filenames;
  DIR *dirp = ::opendir(cache_dir.c_str());
  struct dirent *file;

  if(dirp == NULL) {
    cout << "UNABLE TO SYNC -- CANNOT OPEN CACHE DIR!" << endl;
    return -1;
  } 

  while((file = ::readdir(dirp)) != NULL) {
    string filename = file->d_name;
    struct stat st;
    lstat(Util::getCachePath(file->d_name, cache_dir), &st);
   
    char *buffer = "";
 
    if(S_ISDIR(st.st_mode)) { 
      filename += "/"; //make sure we recognize this as a directory
                       //later
 
    }   

    else {
      buffer = Util::slurpFile(Util::getCachePath(file->d_name, cache_dir));
      if(buffer == NULL) {
        cout << "UNABLE TO SYNC -- CANNOT SLURP FILE: " << file->d_name << endl;
        buffer = ""; //hackjob fix
      }
    }

    //standard directories... 
    if(filename == ".." || filename == ".") {
      continue;
    }

    s3->putObject(buffer, filename, "text/plain"); 
  }
   
  return 0;
}

void Pardus::setOperations(struct fuse_operations *pardus_oper) {
  pardus_oper->getattr = this->getattr;
  pardus_oper->readlink = NULL;
  pardus_oper->mknod = NULL;
  pardus_oper->mkdir = NULL;
  pardus_oper->unlink = NULL;
  pardus_oper->rmdir = NULL;
  pardus_oper->symlink = NULL;
  pardus_oper->rename = NULL;
  pardus_oper->link = NULL;
  pardus_oper->chmod = NULL;
  pardus_oper->chown = NULL;
  pardus_oper->truncate = NULL;
  pardus_oper->utime = NULL;
  pardus_oper->open = this->open; 
  pardus_oper->read = this->read; 
  pardus_oper->statfs = NULL;
  pardus_oper->flush = NULL;
  pardus_oper->release = NULL;
  pardus_oper->fsync = NULL;
  pardus_oper->setxattr = NULL;
  pardus_oper->getxattr = NULL;
  pardus_oper->listxattr = NULL;
  pardus_oper->removexattr = NULL;
  //pardus_oper->opendir = this->opendir;
  //pardus_oper->readdir = this->readdir;
  //pardus_oper->releasedir = this->releasedir;
  pardus_oper->fsyncdir = NULL;
  pardus_oper->init = NULL;
  pardus_oper->destroy = NULL;
  pardus_oper->access = NULL;
  pardus_oper->create = NULL;
  pardus_oper->ftruncate = NULL;
  pardus_oper->fgetattr = NULL;
} 

S3Status getCallback(int bufsize, const char *buf, void* callbackData) {
  cout << "Bufsize: " << bufsize << endl;
  cout << "Buf: " << buf << endl;
  return S3StatusOK;  
}

void* Pardus::syncThread(void *p) {
  while(true) {
    if(need_sync) {
      cout << "SYNCING!!!" << endl;

      Util::log("syncThread", "Synced to S3");
      pardus->syncWithS3Dir("");
      //pardus->syncWithS3();

      //update the object list cache
      s3->listObjectsBlocking();
      need_sync = false;
    }

    usleep(500000);
  }

  return NULL; //satisfy the compiler
}

void Pardus::initS3Sync(void) {
  int ret = pthread_create(&s3sync_thread, NULL, &syncThread, NULL);

  if(ret == -1) {
    cerr << "Unable to start S3 syncing thread! Will abort." << endl;
    exit(1);
  } 
}

void Pardus::createDirsInCache(vector<string> dirs) {
  for(unsigned int i = 0; i<dirs.size(); i++) {

    int ret = ::mkdir(Util::getCachePath(dirs[i].c_str(), cache_dir), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    if(ret < 0) {
      cout << "error in creating cache dir: " << strerror(errno) << endl;
    }
  }
}

void Pardus::initChances(vector<string> dirs) {
  //set up dir structure under FSChance
  for(int i = 0; i< dirs.size(); i++) {
    DirChance dc(dirs[i]);
    chances.probabilities.push_back(dc);
  }

  //add root directory
  DirChance dc("/");
  chances.probabilities.push_back(dc);

  //setup files inside dir structures
  //ObjectList *ol = s3->listObjectsBlocking();
  vector<string> flist = s3->listObjectsBlocking();

  for(int i = 0; i<flist.size(); i++) {
    string fname = flist[i];
    string upper_dir = Util::getDirFromFile(fname.c_str());
   
    //if its a directory, we already have a structure for it...
    if(fname == upper_dir) {
      continue;
    } 

    int index = chances.getDCByPath(upper_dir);
    //whoopsies not found
    if(index == -1) {
      Util::log("initChances", "whoops, file not found in structure");
      cout << "Missed, upper_dir: ****" << upper_dir << "***" << endl;
      continue;
    }

    cout << "Fname: " << fname << ", upper_dir: " << upper_dir << endl;
    cout << "Index: " << index << endl;

    chances.probabilities[index].addFC(fname, 0);

    chances.probabilities[index].printProbabilities();

    cout << "!!!!!!!!!!!!!!!!!!!" << endl;
  }


  chances.printDirs();
}

FSChance Pardus::getChances(void) {
  FSChance fc;

  std::ifstream ifs(serializePath.c_str());
  boost::archive::text_iarchive ia(ifs);
  ia >> fc; 

  return fc;
}

//basically has to combine the serialized data
//and the data obtained from S3, in case the 
//former does not include all the info
void Pardus::combineChances(void) {
  FSChance serialized = getChances();

  //loop over data from S3
  for(int i = 0; i < chances.probabilities.size(); i++) {
    DirChance dirS3 = chances.probabilities[i];
    //if any of the DirChances are in the serialized data
    int serialized_index = 0;
    if((serialized_index=serialized.getDCByPath(dirS3.dir_path)) != -1) {
      //merge it to the data from S3
      chances.probabilities[i] = serialized.probabilities[serialized_index];  
    }    
  }

}

void Pardus::dumpChances(void) {
  std::ofstream ofs(serializePath.c_str());

  boost::archive::text_oarchive oa(ofs);
  oa << chances;
}

void Pardus::loadChances(void) {
  std::ifstream ifs(serializePath.c_str());
  
  boost::archive::text_iarchive ia(ifs);

  ia >> chances; 
}

int main (int argc, char **argv) {
  pardus = new Pardus;
  s3 = new PardusS3;

  Util::init();
  Util::prefetchSwitch = false;

  Util::log("MAIN", "STARTING!");
  Util::log("MAIN", Util::getCachePath("/TRASHES", CACHE_DIR));

  pardus->init(CACHE_DIR, MOUNT_DIR);

  s3->init("pardus", "TDlETn/Nm6ynhuFckMkf9n2SNoM5mQ/Sta1E2YCf", "AKIAJHQL33BS66FCVTLA");

  //gotta do this to fill up the cache with *something*
  vector<string> flist = s3->listObjectsBlocking();  

  cout << "FLIST SIZE: " << flist.size() << endl;

  for(int i = 0; i<flist.size(); i++) {
  }
  //cout << ol << endl;

  vector<string> dirs;
  Util::getDirectoriesFromOL(flist, dirs);
  for(unsigned int i = 0; i<dirs.size(); i++) {
    Util::log("MAIN", "Dir: " + dirs[i]);
  }

  pardus->createDirsInCache(dirs);
  pardus->initChances(dirs);

  //TODO fix the sync function (probably needs a rewrite - 1 hr of work)
  //pardus->initS3Sync();

  Pardus::combineChances();

  int fuse_stat = fuse_main(argc, argv, &(pardus->pardus_oper), NULL); 
  Pardus::dumpChances();

  cout << "WHOOPS FUSE MAIN RETURNED, FUSE_STAT: " << fuse_stat << endl;
  cout << "CACHE STATS: " << CacheStats::toString() << endl; 
  s3->teardown(); 
  return 0;
}
