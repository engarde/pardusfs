#include "cache_stats.h"

int CacheStats::cacheHits = 0;
int CacheStats::cacheMisses = 0;

void CacheStats::hit(void) {
  cacheHits++;
}

void CacheStats::miss(void) {
  cacheMisses++;
}

string CacheStats::toString(void) {
  stringstream sst;
  sst << "Cache hits: " << cacheHits;
  sst << ", Cache misses: " << cacheMisses;
  
  return (sst.str());
}
