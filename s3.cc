#include "s3.h"
#include "util.h"
#include "libs3.h"
#include <errno.h>

using namespace std;

S3Status PardusS3::statusG;
S3Protocol PardusS3::protocolG = S3ProtocolHTTP;
S3UriStyle PardusS3::uriStyleG = S3UriStyleVirtualHost;
S3ResponseProperties *PardusS3::response = NULL;
S3ListBucketContent *PardusS3::content_list = (S3ListBucketContent *)malloc(sizeof (S3ListBucketContent));
vector<S3ListBucketContent> PardusS3::content_list_vector;
vector<int> PardusS3::content_length_vector;
vector<string> PardusS3::test_vector;

int PardusS3::content_list_length = -1;

char* PardusS3::buffer = NULL;
int PardusS3::bufsize = -1;

void PardusS3::init(string bucket_name, string asak, string aaki)  {
  const char *hostname = NULL;

  setBucketName(bucket_name);

  S3_initialize("s3", S3_INIT_ALL, hostname);

  this->awsSecretAccessKey = asak;
  this->awsAccessKeyId = aaki; }


string PardusS3::calcSignature(string method, string content_type, string date, 
  curl_slist *headers, string resource) {

  int ret;
  int bytes_written;
  int offset;
  int write_attempts = 0;

  string Signature;
  string StringToSign;
  StringToSign += method + "\n";
  StringToSign += "\n"; // md5
  StringToSign += content_type + "\n";
  StringToSign += date + "\n";
  int count = 0;
  if(headers != 0) { do { if(strncmp(headers->data, "x-amz", 5) == 0) {
        ++count;
        StringToSign += headers->data;
        StringToSign += 10; // linefeed
      }
    } while ((headers = headers->next) != 0);
  }

  StringToSign += resource;

  const void* key = awsSecretAccessKey.data();
  int key_len = awsSecretAccessKey.size();
  const unsigned char* d = reinterpret_cast<const unsigned char*>(StringToSign.data());
  int n = StringToSign.size();
  unsigned int md_len;
  unsigned char md[EVP_MAX_MD_SIZE];

  HMAC(evp_md, key, key_len, d, n, md, &md_len);

  BIO* b64 = BIO_new(BIO_f_base64());
  BIO* bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);

  offset = 0;
  for(;;) {
    bytes_written = BIO_write(b64, &(md[offset]), md_len);
    write_attempts++;
    // -1 indicates that an error occurred, or a temporary error, such as
    // the server is busy, occurred and we need to retry later.
    // BIO_write can do a short write, this code addresses this condition
    if(bytes_written <= 0) {
      // Indicates whether a temporary error occurred or a failure to
      // complete the operation occurred
      if ((ret = BIO_should_retry(b64))) {
        // Wait until the write can be accomplished
        if(write_attempts <= 10)
          continue;

        // Too many write attempts
        BIO_free_all(b64);
        Signature.clear();
        return Signature;
      } else {
        // If not a retry then it is an error
        BIO_free_all(b64);
        Signature.clear();
        return Signature;
      }
    }
  
    // The write request succeeded in writing some Bytes
    offset += bytes_written;
    md_len -= bytes_written;
  
    // If there is no more data to write, the request sending has been
    // completed
    if(md_len <= 0)
      break;
  }

  // Flush the data
  ret = BIO_flush(b64);
  if ( ret <= 0) { 
    BIO_free_all(b64);
    Signature.clear();
    return Signature;
  } 

  BUF_MEM *bptr;

  BIO_get_mem_ptr(b64, &bptr);

  Signature.resize(bptr->length - 1);
  memcpy(&Signature[0], bptr->data, bptr->length-1);

  BIO_free_all(b64);

  return Signature;
}

void PardusS3::setBucketName(string bucket_name) {
  this->bucketName = bucket_name;
}

string PardusS3::getBucketName(void) {
  return (this->bucketName);
}

string PardusS3::getHostURL(string objectpath) { 

  string host_url = "http://" + bucketName + "." + "s3.amazonaws.com/" + objectpath;

  return host_url;
}

string PardusS3::getDateString(void) {
  return Util::getDate();
}

curl_slist* PardusS3::setGeneralHeaders(curl_slist *headers) {
  headers = curl_slist_append(headers, "User-Agent: pardus");

  curl_slist_append(headers, "Content-Type: ");

  string date_header = "Date: " + getDateString();
  headers = curl_slist_append(headers, date_header.c_str());

  return headers;
}

string PardusS3::getAuthorizationHeader(string objectpath, string content_type, curl_slist *headers) {

  string authorization = "Authorization: AWS " + awsAccessKeyId + ":" 
        + calcSignature("DELETE", content_type, getDateString(), headers, Util::urlEncode(getHostURL(objectpath)));

  return authorization;
}

void PardusS3::responseCompleteCallback(S3Status status, const S3ErrorDetails *error,
                                                void *callbackData) {

  (void) callbackData;

  statusG = status;

  if(error != NULL && error->message != NULL) {
    cout << "ERROR: ";
    cout << error->message << endl; 
  } 

}

void PardusS3::resetS3Status(void) {
  statusG = S3StatusOK;
}

int PardusS3::putObjectDataCallback(int buffersize, char *buffer, void *callback_data) {
  PutData *pd = (PutData *)callback_data;

  if(strlen(pd->data) == 0) {
    return 0;
  }
  
  if(strlen(pd->data) >= (unsigned) buffersize) {
    strncpy(buffer, pd->data, buffersize);
    pd->data = pd->data + buffersize;     
  }

  else {
    strcpy(buffer, pd->data);
  } 

  return (strlen(buffer)+1);
}

S3Status PardusS3::responsePropertiesCallback(const S3ResponseProperties *properties, void *callbackData) {

  response = (S3ResponseProperties *)properties;
  
  return S3StatusOK;
}

//TODO content_type isn't used, figure out whether or not it is possible
void PardusS3::putObject(string data, string objectpath, string content_type) {

  S3BucketContext bucketContext =
  {
    0,
    bucketName.c_str(),
    protocolG,
    uriStyleG,
    awsAccessKeyId.c_str(),
    awsSecretAccessKey.c_str()
  };

  S3PutObjectHandler putObjectHandler =
  {
      {&(PardusS3::responsePropertiesCallback), &(PardusS3::responseCompleteCallback)},
      &(PardusS3::putObjectDataCallback)
  };

  S3PutProperties putProperties; //TODO again, MD5 might be helpful here along with some other options
  memset(&putProperties, 0, sizeof putProperties);
  
  /*
  putProperties.contentType = 0;
  putProperties.md5 = 0;
  putProperties.expires = -1;
  putProperties.cacheControl = 0;
  putProperties.contentDispositionFilename = 0;
  putProperties.contentEncoding = 0;
  putProperties.cannedAcl = S3CannedAclPrivate;
  putProperties.metaDataCount = 0;*/
 
  PutData pd;
  pd.data = (char *)data.c_str();
  pd.length = strlen(pd.data); //TODO figure out how in the world this pointer arithmetic works
   
  const char *key = objectpath.c_str();
 
  do {
    S3_put_object(&bucketContext, key, pd.length, &putProperties, 0,
                      &putObjectHandler, &pd);
  } while (S3_status_is_retryable(statusG));

  //check for errors   
  if(statusG != S3StatusOK) {
    cerr << "WHOOPS GETTING FROM S3 FAILED" << endl;
  } 
}

S3Status PardusS3::getObjectBlockingCallback(int buf_size, const char *buf, void * callback_data) {

  bufsize = buf_size;
  cout << "BUFSIZE: " << bufsize << endl;

  buffer = (char *)buf;
 
  return S3StatusOK;
}

char* PardusS3::getObjectBlocking(string objectpath, long bytes) {
  int ret = getObject(objectpath, bytes, &getObjectBlockingCallback);

  if(ret == -1) {
    return NULL;
  }

  while(true) {
    if(this->buffer != NULL && this->bufsize != -1) {
      char *t = (char *)malloc(this->bufsize);
      strcpy(t, this->buffer);

      this->buffer = NULL;
      this->bufsize = -1;
    
      return t;
    }
  }
   
  return NULL;

}
 
int PardusS3::getObject(string objectpath, long bytes, S3Status (*dataCallback)(int, const char *, void*)) {

  S3BucketContext bucketContext =
  {
    0,
    bucketName.c_str(),
    protocolG,
    uriStyleG,
    awsAccessKeyId.c_str(),
    awsSecretAccessKey.c_str() 
  };

  S3GetConditions getConditions =
  {
    -1,
    -1,
    NULL, /* TODO maybe MD5 checksum would be good here? */
    NULL
  };    

  S3GetObjectHandler getObjectHandler =
  {
    {0, &(PardusS3::responseCompleteCallback)}, 
    dataCallback
  };
 
  do {
    S3_get_object(&bucketContext, objectpath.c_str(), &getConditions, 0, bytes, 0, &getObjectHandler, NULL);
  } while (S3_status_is_retryable(statusG));

  if(statusG != S3StatusOK) {
    cerr << "WHOOPS GETTING FROM S3 FAILED" << endl;
    return -1;
  }    
  return 0;
}

S3ResponseProperties* PardusS3::objectHeaders(string objectpath) {
  S3BucketContext bucketContext = {
    0,
    bucketName.c_str(),
    protocolG,
    uriStyleG,
    awsAccessKeyId.c_str(),
    awsSecretAccessKey.c_str()
  };

  S3ResponseHandler responseHandler = {
    &(PardusS3::responsePropertiesCallback),
    &(PardusS3::responseCompleteCallback)
  };

  do {
    S3_head_object(&bucketContext, objectpath.c_str(), 0, &responseHandler, 0);
  } while(S3_status_is_retryable(statusG));

  /* 
  time_t start_time = time(NULL);
  time_t now_time = time(NULL);

  while(response == NULL) {
    //do nothing 
    if(now_time - start_time > MAX_WAIT) {
      return NULL;
    }        
  } */
  
  //check for errors   
  if(statusG != S3StatusOK) {
    cerr << "WHOOPS HEAD REQUEST TO S3 FAILED" << endl;
  } 

  return response;
}

void PardusS3::deleteObject(string objectpath, string content_type) {
  S3BucketContext bucketContext = {
    0,
    bucketName.c_str(),
    protocolG,
    uriStyleG,
    awsAccessKeyId.c_str(),
    awsSecretAccessKey.c_str()    
  };

  S3ResponseHandler responseHandler =
  {
    0,
    &(PardusS3::responseCompleteCallback)
  };
  
  do {
    S3_delete_object(&bucketContext, objectpath.c_str(), 0, &responseHandler, 0);
  } while(S3_status_is_retryable(statusG));

  if(statusG != S3StatusOK) {
    cerr << "DARN, THERE WAS AN ERROR" << endl;
  } 

}

S3Status PardusS3::getObjectToFileCallback(int buf_size, const char *buf, void * callback_data) {
  FILE *fh = (FILE *)callback_data;
  fputs(buf, fh);
  fflush(fh);

  return S3StatusOK;
}
 
int PardusS3::getObjectToFile(string objectpath, string filepath) {
  FILE *fh = fopen(filepath.c_str(), "wb+");

  if(fh == NULL) {
    cout << "ERROR OPENING FILE: " << strerror(errno) << endl;
  }

  S3BucketContext bucketContext =
  {
    0,
    bucketName.c_str(),
    protocolG,
    uriStyleG,
    awsAccessKeyId.c_str(),
    awsSecretAccessKey.c_str() 
  };

  S3GetConditions getConditions =
  {
    -1,
    -1,
    NULL, /* TODO maybe MD5 checksum would be good here? */
    NULL
  };    

  S3GetObjectHandler getObjectHandler =
  {
    {0, &(PardusS3::responseCompleteCallback)}, 
    &getObjectToFileCallback 
  };
 
  do {
    S3_get_object(&bucketContext, objectpath.c_str(), &getConditions, 0, 0, 0, &getObjectHandler, fh);
  } while (S3_status_is_retryable(statusG));

  if(statusG != S3StatusOK) {
    cerr << "WHOOPS GETTING FROM S3 FAILED" << endl;
    return -1;
  }    

  return 0;
}

S3Status PardusS3::listBucketCallback(int isTrun, const char *nextMarker,
                                      int contentsCount,
                                      const S3ListBucketContent *contents,
                                      int commonPrefixesCount,
                                      const char **commonPrefixes,
                                      void *callbackData) {

  //memcpy(content_list, contents, (sizeof (S3ListBucketContent)));
  //TODO probably leaking memory like no other
  //content_list = (S3ListBucketContent *)calloc(contentsCount, sizeof (S3ListBucketContent));

  cout << "listBucketCallback: !!!!!!!!!!!!!" << endl;
  cout << "CONTENTS COUNT : " << contentsCount << endl;

  for(int i = 0; i<contentsCount; i++) {
    //cout << "name: " << contents[i].key << endl;

    //content_list[i] = contents[i];
    string skey(contents[i].key);
    //S3ListBucketContent lbc = contents[i];
    
    if(skey != "") {
      test_vector.push_back(skey);
      //content_list_vector.push_back(lbc);
    }

  }

  for(int i = 0; i<content_list_vector.size(); i++) {
    cout << "INAME: " << content_list_vector[i].key << endl;
  }

  content_length_vector.push_back(contentsCount);

  if(content_list_length == -1) {
    content_list_length = contentsCount;
  }
  else {
    content_list_length += contentsCount;
  }


  cout << "!!!!!!!!!!!!!" << endl;

  return S3StatusOK;
}

vector<string> PardusS3::listObjectsBlocking(void) {

  S3BucketContext bucketContext =
  {
    0,
    bucketName.c_str(),
    protocolG,
    uriStyleG,
    awsAccessKeyId.c_str(),
    awsSecretAccessKey.c_str() 
  };

  S3ListBucketHandler listBucketHandler =
  {
    { &responsePropertiesCallback, &responseCompleteCallback},
    &listBucketCallback
  };

  do {
    S3_list_bucket(&bucketContext, NULL, NULL, NULL, 0, NULL, &listBucketHandler, NULL);
  } while(S3_status_is_retryable(statusG));  

  if(content_list_length != -1) {
    for(int i = 0; i<content_list_vector.size(); i++) {
      cout << "FNAME: " << test_vector[i] << endl;
    }

  /*
  S3ListBucketContent *ret = (S3ListBucketContent *)calloc(content_list_length, (sizeof (S3ListBucketContent)));

      //S3ListBucketContent *ret = (S3ListBucketContent *)malloc(sizeof (S3ListBucketContent) * content_list_length);     

      int ret_index = 0;
      for(int i = 0; i<content_list_vector.size(); i++) {
        S3ListBucketContent *m = content_list_vector[i];
        if(m != NULL) {
          int c_length = content_length_vector[i];
          cout << "c_length: " << c_length << endl;

          for(int j = 0; j < c_length; j++) {
            cout << "RET_INDEX: " << ret_index << endl;
            ret[ret_index] = m[j]; 
            ret_index++; 
          }        
        }
      }
    */

      cachedObjectList = test_vector;
      test_vector.clear();

      return cachedObjectList;
 
      //ol->list = &(test_vector);
      //ol->list_length = test_vector.size();

      content_list = NULL;
      content_list_length = -1;
      content_list_vector.clear();

      //cachedObjectList = ol;

      //return ol;
    }

    if(statusG != S3StatusOK) {
      cerr << "WHOOPS LISTING FROM S3 FAILED" << endl;  
      return test_vector;
    }

  return test_vector;
}

ObjectList* PardusS3::listObjectsPrefix(string prefix) {

  S3BucketContext bucketContext =
  {
    0,
    bucketName.c_str(),
    protocolG,
    uriStyleG,
    awsAccessKeyId.c_str(),
    awsSecretAccessKey.c_str() 
  };

  S3ListBucketHandler listBucketHandler =
  {
    { &responsePropertiesCallback, &responseCompleteCallback},
    &listBucketCallback
  };

  do {
    S3_list_bucket(&bucketContext, prefix.c_str(), NULL, NULL, 0, NULL, &listBucketHandler, NULL);
  } while(S3_status_is_retryable(statusG));  

  while(true) {
    cout << "WAITING..." << endl;

    if(content_list != NULL && content_list_length != -1) {
      cout << "CONTENT LIST: ---------" << endl;
      for(int i = 0; i<content_list_length; i++) {
        cout << "N: " << content_list[i].key << endl;
      }
      cout << "----------------" << endl;

      //TODO memory leakage galore!
      ObjectList *ol = (ObjectList *)malloc(sizeof (ObjectList));

      S3ListBucketContent *ret = (S3ListBucketContent *)malloc(sizeof (S3ListBucketContent));     

      ret = content_list;
      //ol->list = test_vector;
      //ol->list_length = content_list_length;

      content_list = NULL;
      content_list_length = -1;

      //cachedObjectList = ol;

      return ol;
    }
    if(statusG != S3StatusOK) {
      cerr << "WHOOPS LISTING FROM S3 FAILED" << endl;
      return NULL;
    }
  }

  return NULL;
}
void PardusS3::teardown(void) {
  S3_deinitialize();
}
